# Included by our Vagrantfile for handling liquidfile AWS EC2 instance
# Please use http://www.vagrantup.com/
Vagrant.configure("2") do |config|

  # Set name of box to its folder name
  name = File.basename(File.expand_path('..',__FILE__))
  projectpath = File.expand_path('../../..',__FILE__)

  config.vm.define name do |instance|
    instance.vm.box = name
    instance.vm.box_url = 'https://github.com/mitchellh/vagrant-aws/raw/master/dummy.box'

    instance.vm.provision :shell, :inline => 'date > /etc/vagrant_provisioned_at'

    config.vm.provider :aws do |aws, override|
      aws.access_key_id = '...'
      aws.secret_access_key = '...'

      aws.region = "us-east-1"

      # More comprehensive region config
      aws.region_config "us-west-2" do |region|
        region.ami = "ami-87654321"
        region.keypair_name = "company-west"
      end
  
      aws.ami = "ami-7747d01e"
  
      override.ssh.username = "ubuntu"
      override.ssh.private_key_path = "PATH TO YOUR PRIVATE KEY"
    end
  end

end
